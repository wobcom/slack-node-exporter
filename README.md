# Slack node exporter

Prometheus exporter for exposing various metrics in Slack.

## Installation

To use this exporter, you’ll need Python, `pip`, and `setuptools` on your
system.

Once you have those requirements on your system, you should be able to
either run:

```
pip install -e git+ssh://git@gitlab.service.wobcom.de/infrastructure/slack-node-exporter.git
```

Or, if you want to clone the repository instead, you can issue the following
commands:

```
git clone git@gitlab.service.wobcom.de:infrastructure/slack-node-exporter.git
cd slack-node-exporter
python setup.py install
```

No matter which alternative you choose, you should end up with an executable
called `slack-exporter`. If you go for a git-based install, you should do
this in a well-known location, for instance `/opt`, such that updating can
reuse that repository. Otherwise you can safely delete the
`slack-node-exporter` directory after installing the service

You can then package it, for instance as a systemd service.

### Updating

Updating can be done by using `git pull`. If you did a Git clone in `/opt` and
created a systemd service, for instance, updating should look as follows:

```
cd /opt/slack-node-exporter/
git pull https://gitlab.service.wobcom.de/infrastructure/slack-node-exporter.git
python3 setup.py install
systemctl restart slack-exporter
```

If you used `pip` directly you’ll be able to use fewer steps:

```
pip install -Ue git+ssh://git@gitlab.service.wobcom.de/infrastructure/slack-node-exporter.git
systemctl restart slack-exporter
```

### Deleting

No matter which installation method you chose, you should be able to delete the
executable from the system using `pip`:

```
pip uninstall slack-node-exporter
```

You might be prompted to confirm the deletion action.

If you have a stray repository directory lying around, you can now go ahead and
delete that as well.

## Usage

```
usage: slack-exporter [-h] [--log-level LOG_LEVEL] [--listen-port LISTEN_PORT]
                      [--listen-address LISTEN_ADDRESS] --token TOKEN
                      [--refresh-interval REFRESH]

Slack node exporter for various Slack metrics

optional arguments:
  -h, --help            show this help message and exit
  --log-level LOG_LEVEL
                        Log level, see
                        https://docs.python.org/3/library/logging.html#levels
  --listen-port LISTEN_PORT
                        Listen port
  --listen-address LISTEN_ADDRESS
                        Listen address
  --token TOKEN         Slack token
  --refresh-interval REFRESH
                        Metrics refresh interval in seconds
```

### A word on tokens

Slack tokens are a bit idiosyncratic: you’ll have to create an app
[here](https://api.slack.com/apps), add the appropriate scopes, and then
activate it in the UI (by doing “Add to workspace”). It’s all pretty confusing,
really.

The scopes that this exporter currently needs are:

- `conversations:read`
- `users:read`
- `groups:read`
- `emoji:read`

## Metrics

* Channel count (grouped by whether they’re active and/or public)
* User count (grouped by whether they’re active and/or admins/owners)
* Custom emoji count
