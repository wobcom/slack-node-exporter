"""
The utility module for the collector. Contains the logger and argparser.
"""
import argparse
import logging
import sys

from prometheus_client.core import REGISTRY


LOG_FORMAT = "[%(levelname)s] %(message)s"
DATE_FORMAT = "%Y-%m-%d %H:%M:%S"


def update_all(collectors):
    for collector in collectors:
        collector.update()


def setup_collectors(slk, logger, constructors):
    collectors = []
    for collector in constructors:
        collector = collector(slk, logger)
        REGISTRY.register(collector)
        collectors.append(collector)
    return collectors


def parse_args(args=None):
    """
    creates and argument parser. Optionally takes arguments as a list instead
    of CLI arguments.
    """
    parser = argparse.ArgumentParser(
        description="Slack node exporter for various Slack metrics"
    )
    parser.add_argument(
        "--log-level",
        action="store",
        type=int,
        help="Log level, see https://docs.python.org/3/library/logging.html#levels",
        default=30,
        dest="log_level",
    )
    parser.add_argument(
        "--listen-port",
        action="store",
        type=int,
        help="Listen port",
        default=8000,
        dest="listen_port",
    )
    parser.add_argument(
        "--listen-address",
        action="store",
        type=str,
        help="Listen address",
        default="0.0.0.0",
        dest="listen_address",
    )
    parser.add_argument(
        "--token",
        action="store",
        type=str,
        help="Slack token",
        dest="token",
        required=True,
    )
    parser.add_argument(
        "--refresh-interval",
        action="store",
        type=int,
        help="Metrics refresh interval in seconds",
        default=5 * 60,
        dest="refresh",
    )
    return parser.parse_args(args)


def get_logger(args):
    """
    creates a logger based on the arguments.
    """
    logger = logging.getLogger("topdesk-exporter")
    # see https://docs.python.org/3/library/logging.html#levels
    logger.setLevel(args.log_level)

    # Log to stdout
    stream_handler = logging.StreamHandler(sys.stdout)
    stream_handler.setLevel(args.log_level)
    stream_formatter = logging.Formatter(LOG_FORMAT, DATE_FORMAT)
    stream_handler.setFormatter(stream_formatter)

    logger.addHandler(stream_handler)

    return logger
