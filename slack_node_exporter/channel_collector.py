"""
The channel/conversation collector module.
"""
from collections import Counter

from prometheus_client.core import GaugeMetricFamily

from slack_node_exporter import collector


class Collector(collector.Collector):
    """
    The collector for channels/conversations. Groups by activity and publicity
    status.
    """

    metric = "slack_conversation_count"
    metric_label = "The number of channels/conversations in Slack"

    def conversations(self):
        """gets conversations from Slack."""
        return self.slk.conversations_list(types="public_channel,private_channel")

    def update(self):
        new_metrics = Counter()
        for conversation in self.conversations()["channels"]:
            key = {
                "active": str(not conversation["is_archived"]),
                "public": str(not conversation["is_private"]),
            }
            new_metrics[tuple(key.items())] += 1
        self.metrics = new_metrics
