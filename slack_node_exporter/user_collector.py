"""
The user collector module.
"""
from collections import Counter

from prometheus_client.core import GaugeMetricFamily

from slack_node_exporter import collector


class Collector(collector.Collector):
    """
    The collector for user. Groups by admin/owner and deletion status.
    """

    metric = "slack_user_count"
    metric_label = "The number of users in Slack"

    def users(self):
        """gets users from Slack."""
        return self.slk.users_list(types="public_channel,private_channel")

    def update(self):
        new_metrics = Counter()
        for user in self.users()["members"]:
            key = {
                "active": str(not user["deleted"]),
                "admin": str(user.get("is_admin", False)),
                "owner": str(user.get("is_owner", False)),
            }
            new_metrics[tuple(key.items())] += 1
        self.metrics = new_metrics
