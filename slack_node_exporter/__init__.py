"""
Prometheus exporter for exposing various metrics in TOPdesk.
"""
import time
import sys

from prometheus_client import start_http_server
import slack

from slack_node_exporter import channel_collector, user_collector, emoji_collector, util


def run():
    """
    The main entrypoint for the collector. Sets up the HTTP server and
    collector.
    """
    args = util.parse_args()
    logger = util.get_logger(args)
    slk = slack.WebClient(token=args.token)

    collectors = util.setup_collectors(
        slk,
        logger,
        [
            channel_collector.Collector,
            emoji_collector.Collector,
            user_collector.Collector,
        ],
    )

    start_http_server(args.listen_port, args.listen_address)

    # every so often we update our stats to not run into rate-limiting
    # problems; simple but unclean solution
    i = 0
    while True:
        if i % args.refresh == 0:
            util.update_all(collectors)
            i %= args.refresh
        i += 1
        time.sleep(1)
