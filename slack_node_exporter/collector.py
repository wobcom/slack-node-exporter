"""
The base collector module.
"""
from collections import Counter

from prometheus_client.core import GaugeMetricFamily


class Collector:
    """
    The base collector. All subclasses should implement the function `update` and
    the strings `metric` and `metric_label`.
    """

    def __init__(self, slk, logger):
        self.slk = slk
        self.logger = logger
        self.metrics = Counter()

    def collect(self):
        """
        is the collector entry point. It creates gauges based on the values we
        were provided.
        """
        gauge = GaugeMetricFamily(self.metric, self.metric_label)
        for key, count in self.metrics.items():
            gauge.add_sample(self.metric, dict(key), count)
        yield gauge
