"""
The custom emoji collector module.
"""
from prometheus_client.core import GaugeMetricFamily

from slack_node_exporter import collector


class Collector(collector.Collector):
    """
    The collector for custom emojis
    """

    metric = "slack_emoji_count"
    metric_label = "The number of custom emojis in Slack"

    def emojis(self):
        """gets emojis from Slack."""
        return self.slk.emoji_list()

    def update(self):
        self.metrics[tuple()] = len(self.emojis()["emoji"])
