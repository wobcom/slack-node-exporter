import os
from setuptools import setup


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname), encoding="utf8").read()


setup(
    name="slack_node_exporter",
    version="0.0.1",
    author="Veit Heller",
    author_email="veit@veitheller.de",
    description="Exports various metrics from Slack to Prometheus",
    license="BSD",
    keywords="slack node exporter prometheus",
    packages=["slack_node_exporter"],
    url="https://gitlab.service.wobcom.de/infrastructure/slack",
    long_description=read("README.md"),
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Utilities",
        "License :: OSI Approved :: BSD License",
    ],
    install_requires=["prometheus_client", "argparse", "slackclient"],
    entry_points={"console_scripts": ["slack-exporter = slack_node_exporter:run"]},
)
