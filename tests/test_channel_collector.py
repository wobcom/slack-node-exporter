from slack_node_exporter import channel_collector


class MockSlack:
    def conversations_list(self, *args, **kwargs):
        return {
            "channels": [
                {"is_private": True, "is_archived": True},
                {"is_private": False, "is_archived": True},
                {"is_private": True, "is_archived": False},
                {"is_private": False, "is_archived": False},
                {"is_private": True, "is_archived": True},
                {"is_private": False, "is_archived": True},
                {"is_private": True, "is_archived": False},
                {"is_private": False, "is_archived": False},
            ]
        }


def test_collector():
    collector = channel_collector.Collector(MockSlack(), None)
    collected = list(collector.collect())

    assert len(collected) == 1
    for element in collected:
        assert element.name == "slack_conversation_count"
        assert element.samples == []

    collector.update()
    collected = list(collector.collect())

    assert len(collected) == 1
    for element in collected:
        assert element.name == "slack_conversation_count"
        for sample in element.samples:
            assert sample.value == 2
            assert set(sample.labels.keys()) == {"public", "active"}
