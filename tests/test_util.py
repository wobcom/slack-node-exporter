from slack_node_exporter import util

TEST_ARGS = ["--token", "test"]


def test_arg_parser():
    args = util.parse_args(args=TEST_ARGS)
    assert args.log_level == 30

    args = util.parse_args(args=TEST_ARGS + ["--log-level", "10"])
    assert args.log_level == 10


def test_logger():
    args = util.parse_args(args=TEST_ARGS)

    logger = util.get_logger(args)
    assert logger.isEnabledFor(30)
    assert not logger.isEnabledFor(10)


class MockCollector:
    def __init__(self, *args):
        self.called = False

    def update(self):
        self.called = True

    def collect(self):
        return []


def test_setup():
    constructors = [MockCollector, MockCollector]
    collectors = util.setup_collectors(None, None, constructors)

    assert len(collectors) == 2

    for collector in collectors:
        assert collector.__class__ == MockCollector


def test_updater():
    collectors = [MockCollector(), MockCollector()]

    for collector in collectors:
        assert collector.called == False

    util.update_all(collectors)

    for collector in collectors:
        assert collector.called == True
