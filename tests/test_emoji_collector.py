from slack_node_exporter import emoji_collector


class MockSlack:
    def emoji_list(self, *args, **kwargs):
        # those dicts are references (dangerous!), but for mocking it’s fine
        return {"emoji": [{}] * 10}


def test_collector():
    collector = emoji_collector.Collector(MockSlack(), None)
    collected = list(collector.collect())

    assert len(collected) == 1
    for element in collected:
        assert element.name == "slack_emoji_count"
        assert element.samples == []

    collector.update()
    collected = list(collector.collect())

    assert len(collected) == 1
    for element in collected:
        assert element.name == "slack_emoji_count"
        for sample in element.samples:
            assert sample.value == 10
            assert set(sample.labels.keys()) == set()
