from slack_node_exporter import collector


TEST = {(("key", "val"),): 12}


class MockCollector(collector.Collector):
    metric = "mock"
    metric_label = "Mock metric"

    def update(self):
        self.metrics = TEST


def test_collector():
    collector = MockCollector(None, None)
    collected = list(collector.collect())

    assert len(collected) == 1
    for element in collected:
        assert element.name == "mock"
        assert element.samples == []

    collector.update()
    collected = list(collector.collect())

    assert len(collected) == 1
    for element in collected:
        assert element.name == "mock"
        assert element.samples[0].value == 12
        assert element.samples[0].labels == {"key": "val"}
