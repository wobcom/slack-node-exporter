from slack_node_exporter import user_collector


class MockSlack:
    def users_list(self, *args, **kwargs):
        return {
            "members": [
                {"is_owner": False, "is_admin": True, "deleted": True},
                {"is_owner": False, "is_admin": False, "deleted": True},
                {"is_owner": False, "is_admin": True, "deleted": False},
                {"is_owner": False, "is_admin": False, "deleted": False},
                {"is_owner": False, "is_admin": True, "deleted": True},
                {"is_owner": False, "is_admin": False, "deleted": True},
                {"is_owner": False, "is_admin": True, "deleted": False},
                {"is_owner": False, "is_admin": False, "deleted": False},
            ]
        }


def test_collector():
    collector = user_collector.Collector(MockSlack(), None)
    collected = list(collector.collect())

    assert len(collected) == 1
    for element in collected:
        assert element.name == "slack_user_count"
        assert element.samples == []

    collector.update()
    collected = list(collector.collect())

    assert len(collected) == 1
    for element in collected:
        assert element.name == "slack_user_count"
        for sample in element.samples:
            assert sample.value == 2
            assert set(sample.labels.keys()) == {"active", "admin", "owner"}
